# Triple Health Monitor #
This repository is for ELEC5619--OO Framework    

This project aims to build a web application to allow user to track their data of activities, food, sleep time,etc

All the information about this project is located at the [wiki page](https://bitbucket.org/Feiyang_Ye/2015-oo-framework/wiki/Home)


### Group Members ###
* Yanwen Li
* Anyuan Yu
* Feiyang Ye