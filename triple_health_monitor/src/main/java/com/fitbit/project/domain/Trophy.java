package com.fitbit.project.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Trophy")
public class Trophy implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
		
	@ManyToOne
	@JoinColumn(name="User_Id", nullable=false)
	private User user;
	
	@Column(name="TrophyNo")
	private int trophyNo;
	
	@Column(name="Imgurl")
	private String imgurl;
	
	@Column(name="Description")
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgurl() {
		return imgurl;
	}

	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getTrophyNo() {
		return trophyNo;
	}

	public void setTrophyNo(int trophyNo) {
		this.trophyNo = trophyNo;
	}
	
	
	
}
