package com.fitbit.project.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.project.domain.Activity;
import com.fitbit.project.domain.ActivityAdvice;
import com.fitbit.project.domain.ActivityGoal;
import com.fitbit.project.domain.Trophy;
import com.fitbit.project.domain.User;

@Service(value="activityAdviceManager")
@Transactional
public class ActivityAdviceManager {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	private ActivityGoalManager activityGoalManager;
	
	@Autowired
	private ActivityManager activityManager;
	
	@Autowired
	private TrophyManager trophyManager;
	
	private final Log logger = LogFactory.getLog(getClass());
	
	public ActivityAdvice getAdviceByGoalAndActivity(long id) throws ParseException{
		ActivityAdvice activityAdvice = new ActivityAdvice();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = this.userManager.findByUsername(username);
	    
	    Session currentSession = sessionFactory.getCurrentSession();
	    Activity activity = activityManager.getActivities(id).get(0);
	  //  ActivityGoal activityGoal = activityGoalManager.getGoalByUser(user.getId()).get(activityGoalManager.getGoalByUser(user.getId()).size()-1);   
	  //  Activity activity = activityManager.getTodayAcivity(id);
	    ActivityGoal activityGoal = activityGoalManager.getLastestGoal(id);
	    Date lastDay = dataFormat(activity.getDate());
	    Date today = dataFormat(new java.util.Date());
	    if(lastDay.before(today)){
	    	activityAdvice.setDescription("Oh my god! What a lazy man!!");
	    	
	    }
//	    if(activity.getDate()==null){
//	    	activityAdvice.setDescription("Oh my god! What a lazy man!!");
//	    	
//	    }
	    else{
	    	if(activity.getDistance() >= activityGoal.getDistance()){
	    		if(activity.getSteps() >= activityGoal.getSteps()){
	    			if(activity.getFloors() >= activityGoal.getFloors()){
	    				activityAdvice.setDescription("Congratulation!! You got a Trophy, check out!");
	    				trophyManager.setTrophies(id, 1);
	    			}else{
	    				activityAdvice.setDescription("Just need a little bit more floors");
	    			}
	    		}
	    		else{
	    			activityAdvice.setDescription("Hurry up, you almost finish your goal if more steps.");
	    		}
	    		
	    	}
	    	else{
	    		activityAdvice.setDescription("Oh my friend, you donot reach your personal goal.");
	    	}
	    }

	    
		return activityAdvice;
	}
	public Date dataFormat (Date date) throws ParseException{
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 String dd = sdf.format(date);
		 date = sdf.parse(dd);
		 return date;
	}

}
