package com.fitbit.project.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.project.domain.Activity;
import com.fitbit.project.domain.ActivityGoal;
import com.fitbit.project.domain.User;



@Service(value="activityManager")
@Transactional
public class ActivityManager {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserManager userManager;
	
	private final Log logger = LogFactory.getLog(getClass());
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	
	public void importActivityData(Activity activity) throws ParseException{
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = this.userManager.findByUsername(username);
	    Date today = dataFormat(new java.util.Date());
	    activity.setDate(today);
	    activity.setUser(user);
		this.sessionFactory.getCurrentSession().save(activity);
		
	}
	
	
	public List<Activity> getActivities(long id) throws ParseException{

	    Session session = sessionFactory.getCurrentSession();
	    List<Activity> activity = session.createQuery("From Activity where User_Id=? Order by date Desc").setParameter(0, id).list();
		
		if(activity.size()==0){
				activity.add(new Activity());
				return activity;
				
		}
		return activity; 
	}
	
	public Activity getTodayAcivity(long id) throws ParseException{
		
		Session session = sessionFactory.getCurrentSession();
		Date today = dataFormat(new java.util.Date());
		Activity activity;
	    int result = session.createQuery("From Activity where Date=? and User_Id=?")
	    		.setParameter(0,today)
	    		.setParameter(1, id).list().size();
	    if(result==0){
	    	 activity = null;
	    	
	    }else{
	    	
	    	activity = (Activity) session.createQuery("From Activity where Date=? and User_Id=?")
		    		.setParameter(0,today)
		    		.setParameter(1, id)
		    		.list()
		    		.get(0);
	    }
	    
	    logger.info(activity);
	    logger.info(today);
		return activity;
	}
	
	
	
	
	public Date dataFormat (Date date) throws ParseException{
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 String dd = sdf.format(date);
		 date = sdf.parse(dd);
		 return date;
	}
		
	
	@SuppressWarnings("unchecked")
	public List<Activity> getActivityTimeData(String from, String to) throws ParseException{
		logger.info("______________________");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = this.userManager.findByUsername(username);
		List<Activity> activities = new ArrayList<Activity>();

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		if (from != "" && to != "") {
			Date dateFrom = df.parse(from);
			Date dateTo = df.parse(to);
			logger.info("______________________2");

			activities = this.sessionFactory.getCurrentSession()
					.createQuery("select Floors from Activity where User_Id=?")
					.setParameter(0, user.getId())
					//.setParameter(1, dateFrom)
					//.setParameter(2, dateTo)
					.list();
			//activities = this.sessionFactory.getCurrentSession().createQuery("Select Date From Activity").list();
			
			logger.info("______________________3");
			System.out.print(df.format(dateTo));
			System.out.print(activities);
			logger.info("______________________4");
		}
		else {
			activities = this.sessionFactory.getCurrentSession()
				.createQuery("from Activity where User_Id=?")
				.setParameter(0, user.getId())
				.list();
		}
		logger.info("______________________5");
		return activities;

	}
//	public List<Activity> getActivities() throws ParseException{
//	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    String username = auth.getName();
//    User user = this.userManager.findByUsername(username);
//	//Hibernate.initialize(user.getActivityGoals());
//    
//	List<Activity> activity = null;
//	List<Activity> activity_1 = null;
//	logger.info(user.getActivities().size());
//	if(user.getActivities().size()==0){
//			activity.add(new Activity());
//			return activity;
//			
//	}
//	else{
//			
//			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//			activity = user.getActivities();
//			
//			for(int i=0;i<activity.size();i++){
//				
//				String date = activity.get(i).getDate().toString();
//				//String tem = df.format(date);
//				Date new_date = df.parse(date);
////				activity.get(i).setDate(new_date);
//				logger.info(new_date);
//				
//			}
//			
//			
//	}
//	return activity; 
//
//}



}
