package com.fitbit.project.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.project.domain.*;

@Service(value="sleepTimeManager")
@Transactional
public class SleepTimeManager {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@SuppressWarnings("unchecked")
	public SleepTime findByDate(User user, String d) throws Exception {
		DateFormat df = new SimpleDateFormat("yy-MM-dd"); 
		Date date = df.parse(d);
		List<SleepTime> times = sessionFactory.getCurrentSession()
				.createQuery("from SleepTime where User_Id=? and DATE(date)=?")
				.setParameter(0, user.getId())
				.setParameter(1, date)
				.list();
		
		if (times.size() > 0)
			return times.get(0);
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SleepTime> findUserSleepData (User user, String from, String to) throws Exception {
		List<SleepTime> times = new ArrayList<SleepTime>();
		DateFormat df = new SimpleDateFormat("yy-MM-dd"); 
		if (from != "" && to != "") {
			Date dateFrom = df.parse(from);
			Date dateTo = df.parse(to);
			times = sessionFactory.getCurrentSession()
					.createQuery("from SleepTime where User_Id=? and date>=? and date<=?")
					.setParameter(0, user.getId())
					.setParameter(1, dateFrom)
					.setParameter(2, dateTo)
					.list();
		}
		else {
			times = sessionFactory.getCurrentSession()
				.createQuery("from SleepTime where User_Id=?")
				.setParameter(0, user.getId())
				.list();
		}

		return times;
	}
	
	public List<SleepTimeChart> getSleepTimeData(User user, String from, String to) throws Exception {
		List<SleepTime> times = findUserSleepData(user, from, to);
		List<SleepTimeChart> data = new ArrayList<SleepTimeChart>();
		for (SleepTime time : times) {
			data.add(new SleepTimeChart(time.getDate().getTime(), time.getDuration()));
		}
		return data;
	}
	
}
