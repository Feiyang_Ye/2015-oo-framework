package com.fitbit.project.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.project.domain.Trophy;
import com.fitbit.project.domain.User;

@Service(value="trophyManager")
@Transactional
public class TrophyManager {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserManager userManager;
	
	private final Log logger = LogFactory.getLog(getClass());
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public List<Trophy> getTrophies(long id){
		
	    Session currentSession = this.sessionFactory.getCurrentSession();
	    List <Trophy> trophies = currentSession.createQuery("From Trophy where User_Id=? Order by TrophyNo Asc")
	    		.setParameter(0, id)
	    		.list();

	    logger.info("____________");
	    logger.info(trophies);
	    return trophies;
		
	}
	
	public void setTrophies(long userid, int trophyid){
		 User user = this.userManager.findByUserId(userid);
		 Session currentSession = this.sessionFactory.getCurrentSession();
		 Trophy trophy = new Trophy();
		 if(trophyid==1){
			 trophy.setUser(user);
			 trophy.setTrophyNo(1);
			 trophy.setImgurl("/resources/img/1.jpg");
			 trophy.setDescription("Get World Cup! get World!");
			 
		 }else if(trophyid==2){
			 trophy.setUser(user);
			 trophy.setTrophyNo(2);
			 trophy.setImgurl("/resources/img/2.jpg");
			 trophy.setDescription("You own pricess");
			 
		 }else{
			 
			 
		 }
		 currentSession.save(trophy);
	}
	
	
}
