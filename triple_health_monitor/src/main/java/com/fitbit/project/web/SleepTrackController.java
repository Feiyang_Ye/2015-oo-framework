package com.fitbit.project.web;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fitbit.project.domain.SleepTime;
import com.fitbit.project.domain.SleepTimeChart;
import com.fitbit.project.domain.User;
import com.fitbit.project.service.SleepTimeManager;
import com.fitbit.project.service.UserManager;


@Controller
@RequestMapping(value = "/")
public class SleepTrackController {
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	private SleepTimeManager sleepTimeManager;
	
	@RequestMapping(value = "/sleeptime")
	public ModelAndView handleRequest() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = userManager.findByUsername(username);
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    
	    String yesterdaydate = sdf.format(new Date(System.currentTimeMillis()-24*60*60*1000));
	    
	    SleepTime sleepTime = sleepTimeManager.findByDate(user, yesterdaydate);
	    
		String displayName = createDisplayName();
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("displayName", displayName);

		if (sleepTime != null) {
			DecimalFormat df = new DecimalFormat("#.0");
			double goal = sleepTime.getGoal() * 1.0 / 60;
			double yesterday = sleepTime.getDuration() * 1.0 / 60;
			myModel.put("username", username);
			myModel.put("goal", df.format(goal));
			myModel.put("time", df.format(yesterday));
			if (yesterday >= goal)
				myModel.put("key", 1);
			else
				myModel.put("key", 0);
		}
		return new ModelAndView("sleeptime", myModel);
	}
	
	@RequestMapping(value = "/sleeptime/getdata")
	@ResponseBody
	public List<SleepTimeChart> getData(@RequestParam("from") String from, @RequestParam("to") String to) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = userManager.findByUsername(username);
		return sleepTimeManager.getSleepTimeData(user, from, to);
	}
	
	@RequestMapping(value = "/sleeptime/setting")
	public ModelAndView setting(HttpSession session) {
		String displayName = createDisplayName();
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("displayName", displayName);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = userManager.findByUsername(username);
	    myModel.put("user", user);
	    
		return new ModelAndView("sleeptimesetting", myModel);
	}
	
	@RequestMapping(value = "/sleeptime/savesetting", method=RequestMethod.POST)
	public String saveSetting(HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		User user = userManager.findByUserId(id);
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		Float weight = Float.parseFloat(request.getParameter("weight"));
		Float height = Float.parseFloat(request.getParameter("height"));
		Integer sleepGoal = Integer.parseInt(request.getParameter("sleepgoal"));
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setWeight(weight);
		user.setHeight(height);
		user.setSleepGoal(sleepGoal);
		user.setPassoword("");
		userManager.updateUser(user);
		return "redirect:/sleeptime/setting";
	}
	
	public String createDisplayName(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = userManager.findByUsername(username);
	    if (user == null){
	    	return null;
	    }
	    String firstName = user.getFirstName();
	    String lastName = user.getLastName();
	    String displayName = username;
	    if ((firstName != null && firstName != "") && (lastName != null && lastName != "")){
	    	displayName = firstName + lastName;
	    }
	    else if (firstName != null && firstName != ""){
	    	displayName = firstName;
	    }
	    else if (lastName != null && lastName != ""){
	    	displayName = lastName;
	    }
	    return displayName;
	}	

}
