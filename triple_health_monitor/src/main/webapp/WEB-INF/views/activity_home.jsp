<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>Activity</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/template/css/carousel.css" />" rel="stylesheet">
  	<style>
    	.jumbotron {
    		position: relative;
   			width: 100%;
    		height: 100%;
    		/* background-size:100% 160%;  */
    		background-size: cover;
    		/* background-repeat: repeat; */
    		overflow: hidden;
    		margin-bottom: 0px;
    	}
    	.list-group-item{
    		width:150px;
    	}
    </style>
 </head>
  <body> 
   	<%@ include file="/resources/template/jsp/activity_nar_bar.jsp" %>
   <div class="jumbotron" style="background-image: url('<c:url value="/resources/img/activity.jpg"/>')">     
   <div class="container marketing" style="margin-top: 100px; margin-bottom: 200px;">
		
      <!-- Three columns of text below the carousel -->
      <div class="row">
      	<div class="col-sm-10">
      			<div class="embed-responsive embed-responsive-16by9">
 					 <iframe class="embed-responsive-item" width="460" height="215" src="https://www.youtube.com/embed/3ZBXldCxZEA" frameborder="0" allowfullscreen></iframe>	
				</div>
      	</div>
      	<div class="list-group col-sm-2"  style="left: 20px;">
       				<a class="list-group-item active" href="#" role="button">Menu</a>
		          	<a class="list-group-item" href="/project/activity/goal" role="button">Goal</a>
       			 
          			<a class="list-group-item" href="/project/activity/activity" role="button">Activity</a>
        		
          			<a class="list-group-item" href="/project/activity/trophy_room" role="button">Trophy</a>
        		
      
 	 		
			</div>
		</div>
	</div>
 	</div>
	<%@ include file="/WEB-INF/views/headers/footer.jsp" %>   	
  </body>
</html>