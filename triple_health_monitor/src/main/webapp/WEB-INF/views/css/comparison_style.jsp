<style>
	
	#mw_food {
		width: 90%;
		height: 82%;
	}
	
	#mw_comparison {
		width: 100%;
		height: 100%;
		border: 1px solid black;
	}
	
	#mw_comparison td {
		text-align: center;
		border: 1px solid black;
	}
	
	.section_title {
		font-size: 28;
		height: 20%;
	}
	
	#chart {
		width: 55%;
		vertical-align: text-top;
	}
	
	#datePicker {
		width: 150px;
		padding: 0px;
	}
	
	#datepicker {
		width: 120px;
	}
		
	#suggestion {
		height: 70%;
	}
	
	#chart_controller {
		width: 25%;
	}
		
	.date_pick_display {
		margin: 10px;
	}
	
	#chartContainer {
		width: 90%;
		margin: auto;
	}
	
</style>