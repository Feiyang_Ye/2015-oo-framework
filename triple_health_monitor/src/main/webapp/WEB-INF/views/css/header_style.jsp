<style>
		
	#head_banner {
		background: #66CCCC;
		width: 100%;
		height: 10%;
	}
	
	#head_banner td {
		width: 33%;
	}
	
	.logo {
		font-family: "Cracked";
		font-size: 42px;
		color: #666666;
		padding-left: 20px;
	}
	
	#header_banner, #title {
		font-family: "Curlz MT";
		color: #009900;
		font-size: 48;
	}
	
	#navigate_bar {
		width: 10%;
		height: 90%;
		position: absolute;
		left: 0;
		top: 10%;
		background: #CCFFFF;
	}
	
	#food_navigate {
		width: 90%;
		height: 8%;
		position: absolute;
		left: 10%;
	}
	
	#food_navigate table{
		background: #CCCCCC;
		width: 100%;
		height: 100%;
	}
	
	#food_navigate td {
		text-align: center;
		width: 25%;
		font-family: chalkboard;
		color: #6666CC;
		font-size: 24;
	}
	
	#food_navigate a {
		text-decoration: none;
	}
	
	#main_window {
		position: absolute;
		left: 10%;
		top: 18%;
	}
		
</style>