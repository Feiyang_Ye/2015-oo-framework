<style>
	
	#mw_food {
		width: 90%;
		height: 82%;
	}
	
	#mw_intake {
		width: 100%;
		height: 100%;
		border: 1px solid black;
	}
	
	#mw_intake td{
		text-align: center;
		border: 1px solid black;
	}
	
	.section_title {
		font-size: 28;
		height: 20%;
	}
	
	#table_intake {
		width: 55%;
		vertical-align: text-top;
	}
	
	#table_intake div {
		margin: auto;
	}
	
	#data, #data_header {
		
		font-size: 18;
	}
	
	#table_intake td{
		padding: 10px;
	}
	
	a{
		text-decoration: none;
	}
	
</style>