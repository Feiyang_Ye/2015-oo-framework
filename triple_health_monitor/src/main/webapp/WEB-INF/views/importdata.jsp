<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%> 
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>Import Today's Data</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <%-- <link href="<c:url value="/resources/template/css/carousel.css" />" rel="stylesheet"> --%>
	<link href="<c:url value="/resources/template/css/dashboard.css" />" rel="stylesheet">   
    <link rel="stylesheet" href="resources/css/nv.d3.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  
 </head>
<body>
	
	<%@ include file="/resources/template/jsp/activity_nar_bar.jsp" %>
	<div class="container-fluid">
	<div class="row">
	<div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar" id="wow">
            <li><a href="/project/activity/">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="/project/activity/goal">Set Goal</a></li>
            <li class="active"><a href="/project/activity/activity">Check Activity</a></li>
            <li><a href="/project/activity/trophy_room">Trophy Room</a></li>
          </ul>
          
	</div>

	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	
	<h1 class="page-header">Import Today's Data</h1>
	<br>
	<div class="table-responsive">
	
	<sf:form method="POST" commandName="activity">
		<fieldset>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				
      			<div class="form-group">
    		  		<div class="col-sm-4 control-label"><label for="Distance">Distance</label></div>
    		  		<div class="col-sm-4 input-group">
    		  			<input type="number" class="form-control" id="distance" name="distance">
      			  		<div class="input-group-addon">m</div>
      				</div>
      			</div>
				
				<div class="form-group">
    		  		<div class="col-sm-4 control-label"><label for="Floors">Floors</label></div>
    		  		<div class="col-sm-4 input-group">
		    		  	<input type="number" class="form-control" id="floors" name="floors">
      			  		<div class="input-group-addon">m</div>
      				</div>
      			</div>
      			
      			<div class="form-group">
    		  		<div class="col-sm-4 control-label"><label for="Steps">Steps</label></div>
    		  		<div class="col-sm-4 input-group">
    		  			<input type="number" class="form-control" id="steps" name="steps">
      			  		<div class="input-group-addon">m</div>
      					
      		  		</div>
      			</div>
				
			
      			<div class="form-group">
      				<div class="col-sm-4 control-label">
						<button type="submit" class="btn btn-default">Submit</button>
					</div>
				</div>
			</div>
		</fieldset>
	</sf:form>
	</div>
	</div>
	</div>
	</div>
	 <%@ include file="/WEB-INF/views/headers/footer.jsp" %> 	
</body>
</html>	


<%-- <sf:form method="POST" commandName="activity">
		<fieldset>
			<table>
				<tr>
					<th><label for="goal_caloriesOut">CaloriesOut:</label></th>
					<td><sf:input path="caloriesOut"/></td>
				</tr>
				<tr>
					<th><label for="goal_distance">Distance:</label></th>
					<td><sf:input path="distance"/></td>
				</tr>
				<tr>
					<th><label for="goal_floors">Floors:</label></th>
					<td><sf:input path="floors"/></td>
				</tr>
				<tr>
					<th><label for="goal_steps">Steps:</label></th>
					<td><sf:input path="steps"/></td>
				</tr>
				<tr>
					<th><label for="goal_date">Date:</label></th>
					<td><sf:input path="date"/></td>
				</tr>

				
				<tr>
					<th><a href="activity_home"><button>Cancel</button></a></th>
					<td><sf:hidden path="id"/>
					<td><input type="submit" value="Update"/></td>
				</tr>
			</table>
		</fieldset>
	</sf:form>


</body>
</html>	 --%>