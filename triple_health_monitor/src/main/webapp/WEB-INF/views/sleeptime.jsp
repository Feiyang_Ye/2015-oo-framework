<html>
<head>
	<%@ include file="/WEB-INF/views/css/header_style.jsp" %>
	<%@ include file="/WEB-INF/views/css/mw_style.jsp" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<title>Sleep Time Track</title>
	<link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/template/css/dashboard.css" />" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/nv.d3.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<style>
		.form-control {
			display: inline-block;
		}
		
		.number {
			font-size: 48px;
			color: red;
		}
	</style>
</head>
<body>
	<%@ include file="/resources/template/jsp/sleep_nav_bar.jsp" %>
	<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="/project/sleeptime/setting">Settings</a></li>
          </ul>
        </div>
  		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  		  <div class="page-header">
            <h2>Overview</h2>
          </div>
          <h3>Yesterday, you have slept for <span id="num-yesterday" class="number">0</span> hours.</h3>
          <h3>Your sleep goal is <span id="num-goal" class="number">0</span> hours.</h3>
          <div style="height:20px;"></div>
  			<div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Sleep Time
            </h3>
          </div>
          <div class="panel-body" id="overview" style="height:700px">
          	<div>
          		<form id="form">
          			<input type="text" class="form form-control" style="width:150px" id="from" name="from" /> to 
            		<input type="text" class="form form-control" style="width:150px" id="to" name="to" /> 
            		<input type="submit" class="btn btn-default" value="Go" />
            	</form>
            </div>
            <div style="height:20px"></div>
            <svg></svg>
          </div>
        </div>
		</div>
	  </div>
	  <input type="hidden" id="username" value="${ username }" />
	  <input type="hidden" id="goal" value="${ goal }" />
	  <input type="hidden" id="time" value="${ time }" />
	  <input type="hidden" id="key" value="${ key }" />
	</div>
	
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/d3.min.js"></script>
    <script src="resources/js/nv.d3.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
		$(function() {
			$("#from").datepicker({
				dateFormat: "yy-mm-dd",
				firstDay: 1
			});
			
			$("#to").datepicker({
				dateFormat: "yy-mm-dd",
				firstDay: 1
			});
			
			$.ajax({
				type: "get",
				url: "/project/sleeptime/getdata?from=&to=",
				success: function(response) {
					nv.addGraph(function() {
						var data = [{key:"Sleep time", values:response}];
				    	var chart;
				    	chart = nv.models.multiBarChart()
				    		.x(function(d) { return d.date })
			       			.y(function(d) { return d.duration });
				    
				    	chart.xAxis
				    	.showMaxMin(false)
				    	.tickFormat(function(d) {
				     		return d3.time.format('%x')(new Date(d))
				    	});

				    	chart.yAxis
				      		.tickFormat(d3.format(',f'));
				    
				    	d3.select('#overview svg')
				      		.datum(data)
				      		.transition().duration(500)
				      		.call(chart);
				   	 nv.utils.windowResize(chart.update);
	    		    	return chart;
	    			});		 
				}
			});
			
			if ($('#key').val() == '0') {
				var d = {
					"username" : $('#username').val(),
					"yesterday" : $('#time').val(), 
					"goal" : $('#goal').val()
					};
			
				$.ajax({
					type: "post",
					url: "https://zapier.com/hooks/catch/3im4ij/",
					data: d,
		        	dataType: "json"
				});
			}
			
			$('#form').submit(function() {
				$.ajax({
					type: "get",
					url: "/project/sleeptime/getdata?from="+$("#from").val()+"&to="+$("#to").val(),
					success: function(response) {
						nv.addGraph(function() {
							var data = [{key:"Sleep time", values:response}];
					    	var chart;
					    	chart = nv.models.multiBarChart()
					    		.x(function(d) { return d.date })
				       			.y(function(d) { return d.duration });
					    
					    	chart.xAxis
					    	.showMaxMin(false)
					    	.tickFormat(function(d) {
					     		return d3.time.format('%x')(new Date(d))
					    	});

					    	chart.yAxis
					      		.tickFormat(d3.format(',f'));
					    
					    	d3.select('#overview svg')
					      		.datum(data)
					      		.transition().duration(500)
					      		.call(chart);
					   	 	nv.utils.windowResize(chart.update);
		    		    	return chart;
		    			});		 
					}
				});
				return false;
			});
			
			var yesterday = 0;
			var goal = 0;
			if ($('#time').val() != '' && $('#time').val() !='0')
				yesterday = parseFloat($('#time').val()).toFixed(1);
			if ($('#goal').val() != '' && $('#goal').val() !='0')
				goal = parseFloat($('#goal').val());
				
			$('#num-yesterday').text(yesterday);
			$('#num-goal').text(goal);
		});
	</script>
</body>
</html>