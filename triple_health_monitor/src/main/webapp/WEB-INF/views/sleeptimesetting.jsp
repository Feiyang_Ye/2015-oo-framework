<html>
<head>
	<%@ include file="/WEB-INF/views/css/header_style.jsp" %>
	<%@ include file="/WEB-INF/views/css/mw_style.jsp" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<title>Sleep Time Track</title>
	<link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/template/css/dashboard.css" />" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/nv.d3.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<style>
		.form-control {
			display: inline-block;
		}
		
		.large {
			font-size: 20px;
		}
	</style>
</head>
<body>
	<%@ include file="/resources/template/jsp/sleep_nav_bar.jsp" %>
	<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="/project/sleeptime">Overview</a></li>
            <li class="active"><a href="#">Settings</a></li>
          </ul>
        </div>
  		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  		  <div class="page-header">
            <h2>Settings</h2>
          </div>
  		  <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Setting
            </h3>
          </div>
          <div class="panel-body" id="overview">
          	<div class="col-md-6">
          		<form method="post" action="/project/sleeptime/savesetting">
				<label class="col-md-4 control-label large">First Name</label>
				<c:choose>
				<c:when test="${empty user.firstName}">
				  <div class="col-md-8">
				  <div class="form-group">
					<input type="text" name="firstName" class="form-control input-lg" placeholder="First Name">
				  </div>
				  </div>
				</c:when>
				<c:when test="${not empty user.firstName}">
				  <div class="col-md-8">
				  <div class="form-group">
					<input type="text" name="firstName" class="form-control input-lg" placeholder="First Name" value="${user.firstName}">
				  </div>
				  </div>
				</c:when>
				</c:choose>
				
				<label class="col-md-4 control-label large">Last Name</label>			  
				<c:choose>
				<c:when test="${empty user.lastName}">
				  <div class="col-md-8">
				  <div class="form-group">
					<input type="text" name="lastName" class="form-control input-lg" placeholder="Last Name">
				  </div>
				  </div>
				</c:when>
				<c:when test="${not empty user.lastName}">
				  <div class="col-md-8">
				  <div class="form-group">
					<input type="text" name="lastName" class="form-control input-lg" placeholder="Last Name" value="${user.lastName}">
				  </div>
				  </div>
				</c:when>
				</c:choose>
				
				<label class="col-md-4 control-label large">Weight (kg)</label>
				<c:choose>
			  	  <c:when test="${empty user.weight}">
			  	    <div class="col-md-8">
			  	    <div class="form-group">
			  	      <input type="number" name="weight" class="form-control input-lg" placeholder="Weight" />
			  	    </div>
			  	    </div>
			  	  </c:when>
			  	  <c:when test="${not empty user.weight}">
			  	    <div class="col-md-8">
			  	    <div class="form-group">
			  	      <input type="number" name="weight" class="form-control input-lg" placeholder="Weight" value="${ user.weight }"/>
			  	    </div>
			  	    </div>
			  	  </c:when>
			  	</c:choose>

				<label class="col-md-4 control-label large">Height (cm)</label>
				<c:choose>
			  	  <c:when test="${empty user.height}">
			  	  	<div class="col-md-8">
			  	    <div class="form-group">
					  <input type="number" name="height" class="form-control input-lg" placeholder="Height" />
				  	</div>
				  	</div>
				  </c:when>
				  <c:when test="${not empty user.height}">
				    <div class="col-md-8">
				    <div class="form-group">
					  <input type="number" name="height" class="form-control input-lg" placeholder="Height" value="${ user.height }"/>
				    </div>
				    </div>
				  </c:when>
				</c:choose>
				
				<label class="col-md-4 control-label large">Sleep Goal (min)</label>
				<c:choose>
			  	  <c:when test="${empty user.sleepGoal }">
			  	  	<div class="col-md-8">
			  	    <div class="form-group">
					  <input type="number" name="sleepgoal" class="form-control input-lg" placeholder="Sleep Goal" />
				  	</div>
				  	</div>
				  </c:when>
				  <c:when test="${not empty user.sleepGoal }">
				    <div class="col-md-8">
				    <div class="form-group">
					  <input type="number" name="sleepgoal" class="form-control input-lg" placeholder="Sleep Goal" value="${ user.sleepGoal }"/>
				    </div>
				    </div>
				  </c:when>
				</c:choose>
				<input type="hidden" name="id" value="${ user.id }"/>
				<div class="col-md-8 col-md-offset-4">
				  <input class="btn btn-primary" type="submit" value="Save" />
				</div>
			</form>
			</div>
          </div>
        </div>
		</div>
	  </div>
	</div>
	
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/d3.min.js"></script>
    <script src="resources/js/nv.d3.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</body>
</html>