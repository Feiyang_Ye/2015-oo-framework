<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>Activity</title>
    
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/template/css/carousel.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/template/css/dashboard.css" />" rel="stylesheet"> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	
 
  
  </head>
  <body>
  		<%@ include file="/resources/template/jsp/activity_nar_bar.jsp" %>
  		<div class="container-fluid">
		<div class="row">
		<div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar" id="wow">
            <li><a href="/project/activity/">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="/project/activity/goal">Set Goal</a></li>
            <li class="active"><a href="/project/activity/activity">Check Activity</a></li>
            <li><a href="/project/activity/trophy_room">Trophy Room</a></li>
          </ul>
          
          	<div class="panel-heading"></div>
  			<div id="filter_div"></div>
  		  
        </div>
			
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">	
   		 		<h1 class="page-header">User Activity Diagram</h1>
				  	
				<div class="col-sm-12 panel panel-default" style="height: 600px;">
 					<div class="panel-heading">Diagram</div>
 					<div class="panel-body" id="dashboard_div">
  						<div id="curve_chart" style="width: 699px; height: 500px;" ></div>
  						<div id='date_control'></div>
  					</div>
				</div>
   		 		<div><a href="importdata"><button class="btn btn-primary">Today's Data</button></a>
   		 		</div>
   		 	</div>
   		 </div>
   		</div> 	
  	 <%@ include file="/WEB-INF/views/headers/footer.jsp" %>
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">
   
    google.load('visualization', '1.1', {packages: ['controls']});
    google.setOnLoadCallback(drawChart);
    
    
    function drawChart() {
	
    	// Preapre the data;
        data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', 'Distance');
        data.addColumn('number', 'Floors');
        data.addColumn('number', 'Steps');
        var tem = [<c:forEach items="${activities}" var="entry">
		     				['${entry.date}', ${entry.distance},${entry.floors}, ${entry.steps}],
				</c:forEach>]
     
        for(var i=0;i<tem.length;i++){
        	var ss= tem[i][0].substring(0,10);
        	tem[i][0] =  new Date(ss);
       	 	data.addRow([new Date(ss),tem[i][1],tem[i][2],tem[i][3]]);
       	} 

		
		// Controls data table
        var columnsTable = new google.visualization.DataTable();
        columnsTable.addColumn('number', 'colIndex');
        columnsTable.addColumn('string', 'colLabel');
        var initState= {selectedValues: []};
        // put the columns into this data table (skip column 0)
        for (var i = 1; i < data.getNumberOfColumns(); i++) {
            columnsTable.addRow([i, data.getColumnLabel(i)]);
            initState.selectedValues.push(data.getColumnLabel(i));
        }
        
        // Dashboard object
		var dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div')); 
        // Chart object
        var chart = new google.visualization.ChartWrapper({
            chartType: 'LineChart',
            containerId: 'curve_chart',
            options: {
            	curveType: 'function',
                width: 900,
                height: 500,
                chartArea:{left:70,top:15,width:'70%',height:'90%'}
            }
        });
		
        //chart.draw();
        // Line category control
        var columnFilter = new google.visualization.ControlWrapper({
            controlType: 'CategoryFilter',
            containerId: 'filter_div',
            dataTable: columnsTable,
            options: {
                filterColumnLabel: 'colLabel',
                ui: {
                    label: 'Options',
                    allowTyping: false,
                    allowMultiple: true,
                    selectedValuesLayout: 'belowStacked'
                }
            },
            state: initState
        });
        
       // Time rang control  
        var dateRangeSlider = new google.visualization.ControlWrapper({
            controlType: 'DateRangeFilter',
            containerId: 'date_control',
            options: {
              filterColumnLabel: 'Date',
             /*  'minValue': 1,
              'maxValue': 10 */
            },
            // Explicitly positions the thumbs at position 3 and 8,
            // out of the possible range of 1 to 10.
            //'state': {'lowValue': 3, 'highValue': 8}
          });

       // Bind chart with time rang control
       dashboard.bind(dateRangeSlider, chart);
        
       // Line catrgory control listener  
       google.visualization.events.addListener(columnFilter, 'statechange', function () {
            var state = columnFilter.getState();
            var row;
            var columnIndices = [0];
            for (var i = 0; i < state.selectedValues.length; i++) {
                row = columnsTable.getFilteredRows([{column: 1, value: state.selectedValues[i]}])[0];
                columnIndices.push(columnsTable.getValue(row, 0));
            }
            // sort the indices into their original order
            columnIndices.sort(function (a, b) {
                return (a - b);
            });
            chart.setView({columns: columnIndices});
            chart.draw();
        });

        columnFilter.draw(); 
        dashboard.draw(data);
        
      }
    
  
    
    
    
    
    
    
    
   $(function(){    
    	  
    	  $("#from").datepicker({
				dateFormat: "yy-mm-dd",
				firstDay: 1
			});
			
			$("#to").datepicker({
				dateFormat: "yy-mm-dd",
				firstDay: 1
			});
			
			$('#form').submit(function() {
				$.ajax({
					type: "get",
					url: "/project/activity/activity/getdata?from="+$("#from").val()+"&to="+$("#to").val(),
					success: function(response) {
						console.log(response);
						data.addRows([
										
						      ]);
					}
				});
				return false;
			});	
		
    	  
      });
      
      
    </script> 	
  </body>
</html>
