<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>Activity</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/template/css/carousel.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/template/css/dashboard.css" />" rel="stylesheet"> 
  
    
  </head>
  <body>
  		<%@ include file="/resources/template/jsp/activity_nar_bar.jsp" %>  
  		<div class="container-fluid">
		<div class="row">
  		<div class="col-sm-3 col-md-2 sidebar">
          <div><ul class="nav nav-sidebar" id="wow">
            <li><a href="/project/activity/">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="/project/activity/goal">Set Goal</a></li>
            <li><a href="/project/activity/activity">Check Activity</a></li>
            <li class="active"><a href="/project/activity/trophy_room">Trophy Room</a></li>
          </ul>
          </div>
          <div>
          		<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
 						 Latest Goal
				</button>
				<div class="collapse" id="collapseExample">
  					<div class="well">
   							Distance: ${ activityGoal.distance } 
   							Floors: ${ activityGoal.floors } 
   							Steps: ${ activityGoal.steps }
  					</div>
				</div>
          </div>
		</div>	
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">	
   		 		<h1 class="page-header">Trophy Room</h1>
				<br>
            	           	
            	
				<div class="col-sm-12 panel panel-default" style="height: 1200px;">
 					<div class="panel-body" >
  						<div>
  						
              			<c:if test="${ empty activity }">
								<p class="bg-warning">You did not take activity toady!</p>
						</c:if>
			
						<c:if test="${ not empty activity }">
              					<p class="bg-primary lead">Distance: ${activity.distance} Floors: ${activity.floors} Steps: ${activity.steps}</p>
              			</c:if>
              			
              			<c:if test="${ empty advice }">
								<p class="bg-warning">Sorry, you seems are new right now, import some data firstly.</p>
						</c:if>
			
						<c:if test="${ not empty advice }">
              					<p class="bg-primary lead">${advice.description}</p>
              			</c:if>
            			</div>
            			<div>
            				<c:if test="${ empty trophies }">
            					<p class="bg-warning">There are no trophies.</p>
            				</c:if>
            				<c:if test="${ not empty trophies }">
            				<div class="row">
            				 <c:forEach items="${trophies}" var="entry">
	            				 <div class="col-md-6">
								 		<img src="<c:url value="${entry.imgurl}"/>" alt="${entry.trophyNo}" class="img-circle" style="height: 220px; margin-left: 20px; margin-top: 50px; width: 220px;" data-toggle="popover" title="${entry.trophyNo}" data-placement="buttom" data-content="${entry.description}">
								 		
								 </div>															
							</c:forEach>
							</div>
							</c:if>
            			</div>
  					</div>
				</div>
   		 	
   		 	</div>
		
    </div>
    </div>
  	 <%@ include file="/WEB-INF/views/headers/footer.jsp" %>
  	 <script type="text/javascript">
  	  
  		 $(function () {
  			  $('[data-toggle="popover"]').popover()
  			}) 		
  	 	  
  	 	  

  	 
  	 </script>  	  	
  </body>
</html>
