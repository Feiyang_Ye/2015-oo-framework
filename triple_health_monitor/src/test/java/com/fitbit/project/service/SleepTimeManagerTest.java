package com.fitbit.project.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.project.domain.SleepTimeChart;
import com.fitbit.project.domain.User;

@ContextConfiguration(locations = "classpath:persistence-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class SleepTimeManagerTest {
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	SleepTimeManager sleepTimeManager;
	
	@Test
	@Transactional
	@Rollback(true)
	public void testGetData() throws Exception {
		User user = new User();
		user.setUsername("testAdd");
		user.setPassoword("testPassword");
		user.setFirstName("testAdd");
		userManager.addUser(user);
		user = userManager.findByUsername("testAdd");
		assertNotNull(user);
		
		String from = "2015-09-01";
		String to = "2015-09-30";
		List<SleepTimeChart> result = sleepTimeManager.getSleepTimeData(user, from, to);
		assertNotNull(result);
	}

}
