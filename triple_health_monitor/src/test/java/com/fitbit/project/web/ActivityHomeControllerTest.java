package com.fitbit.project.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.fitbit.project.domain.Activity;
import com.fitbit.project.domain.ActivityAdvice;
import com.fitbit.project.domain.ActivityGoal;
import com.fitbit.project.domain.User;
import com.fitbit.project.service.ActivityAdviceManager;
import com.fitbit.project.service.ActivityGoalManager;
import com.fitbit.project.service.ActivityManager;
import com.fitbit.project.service.ActivityUserManager;
import com.fitbit.project.service.TrophyManager;
import com.fitbit.project.service.UserManager;

import junit.framework.TestCase;

@ContextConfiguration(locations = "classpath:persistence-context-2.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ActivityHomeControllerTest extends TestCase {
	
	@Autowired
	private ActivityGoalManager activityGoalManager;
		
	@Autowired
	private ActivityManager activityManager;
	
//	@Autowired
//	private ActivityAdviceManager activityAdviceManager;
//	
//	@Autowired
//	private TrophyManager trophyManager;
	
//	private static String SEC_CONTEXT_ATTR = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

//	@Autowired
//	private FilterChainProxy springSecurityFilterChain;

//	@Autowired
//	private WebApplicationContext wac;
//
//	private MockMvc mockMvc;
	
	@Test
	@Transactional
	@Rollback(true)
	public void getGoalByUserTest() throws Exception{

		List<ActivityGoal> goals = this.activityGoalManager.getGoalByUser(3);
		assertNotNull(goals);
		
	}
	
	@Test
	@Transactional
	@Rollback(true)
	public void getGoalByIdTest() throws Exception{

		ActivityGoal goals = this.activityGoalManager.getGoalById(19);
		assertNotNull(goals);
		
	}
	
	@Test
	@Transactional
	@Rollback(true)
	public void getLastestGoalTest() throws Exception{

		ActivityGoal testGoal = new ActivityGoal();
		testGoal.setDistance((float) 1000.0);
		testGoal.setFloors(1000);
		testGoal.setSteps(1000);
		ActivityGoal goals = this.activityGoalManager.getLastestGoal(3);
		assertEquals(goals.getDistance(),testGoal.getDistance());
		assertEquals(goals.getFloors(),testGoal.getFloors());
		assertEquals(goals.getSteps(),testGoal.getSteps());

		
	}
	
	@Test
	@Transactional
	@Rollback(true)
	public void getActivitiesTest() throws Exception{
		
		List<Activity> activity = this.activityManager.getActivities(3);
		assertNotNull(activity);
		
	}

	@Test
	@Transactional
	@Rollback(true)
	public void getTodayAcivityTest() throws Exception{
		Activity activityTest = null;
		Activity activity = this.activityManager.getTodayAcivity(2);
		assertEquals(activityTest, activity);
		
	}
	
//	@Test
//	@Transactional
//	@Rollback(true)
//	public void getAdviceByGoalAndActivityTest() throws Exception{
//		
//		ActivityAdvice activityadivce = this.activityAdviceManager.getAdviceByGoalAndActivity(3);
//		assertNotNull(activityadivce);
//		
//	}

	
//	@Test
//	public void userAuthenticates() throws Exception {
//		final String username = "user";
//		mockMvc.perform(post("/j_spring_security_check").param("j_username", username).param("j_password", "password"))
//			.andExpect(redirectedUrl("/"))
//			.andExpect(new ResultMatcher() {
//				public void match(MvcResult mvcResult) throws Exception {
//					HttpSession session = mvcResult.getRequest().getSession();
//					SecurityContext securityContext = (SecurityContext) session.getAttribute(SEC_CONTEXT_ATTR);
//					Assert.assertEquals(securityContext.getAuthentication().getName(), username);
//				}
//			});
//	}
	
}
